import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutterassignment1/animal_text.dart';
import 'package:flutterassignment1/text_control.dart';

class App extends StatefulWidget {
  App({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  static const _animals = const ["dog", "cat", "mouse", "rabbit", "fox"];
  var currentText = "Click the button";

  void _randomizeText() {
    setState(() {
      final random = Random().nextInt(_animals.length - 1);
      currentText = _animals[random];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(currentText),
            TextControl(_randomizeText),
          ],
        ),
      ),
    );
  }
}
