import 'package:flutter/material.dart';

class TextControl extends StatelessWidget {
  final Function _onClick;

  TextControl(this._onClick);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10),
      child: RaisedButton(child: Text("New animal"), onPressed: _onClick),
    );
  }
}
